﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
    interface ISurveyService
    {
        int Add(Survey  survey);

        bool Update(Survey survey);

        Survey GetSurveyById(int id);

        ICollection<Survey> GetLast10Surveys();

        bool ArchiveSurvey(Survey survey); 
        //int Add(User user);

        //User Get(int id);

        //bool Update(User user);

        //bool Delete(int id);

    }
}
