﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
    interface ISurveyQuestionsService
    {
        bool AddQuestionToSurvey(int questionId, int surveyId);
        ICollection<Question> GetQuestionsBySurvey(int surveyId);
     //   bool GetOptionsByQuestionOfSurvey(int surveyId, int questionId);
    }
}
