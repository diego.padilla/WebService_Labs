﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    class QuestionRepository : IQuestionRepository
    {
       
        public int Add(Question question)
        {
            using (var ctx = new SurveyDbContext())
            {
                question.CreatedDate = DateTime.Now;
                question.ModifiedDate = null;
                question.IsActive = true;
                ctx.Questions.Add(question);
                ctx.SaveChanges();

                return question.Id;
            }
        }

        public bool Delete(Question question)
        {
            using (var ctx = new SurveyDbContext())
            {
                ctx.Questions.Remove(question);
                ctx.SaveChanges();
                return true; 
            }
        }

        public Question Get(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Questions.AsNoTracking().SingleOrDefault(x => x.Id == id);
                
            }
        }

        public bool Update(Question question)
        {
            using (var ctx = new SurveyDbContext())
            {
                Question currentQuestion = ctx.Questions.SingleOrDefault(x => x.Id == question.Id);

                if (currentQuestion == null) return false;

                currentQuestion.Id = question.Id;
                currentQuestion.IsActive = question.IsActive;
                currentQuestion.QuestionTypeId = question.QuestionTypeId;
                currentQuestion.ModifiedDate = DateTime.Now;
                ctx.SaveChanges();
                return true; 
            }
        }

        public bool Exist(int id) {
            using (var context = new SurveyDbContext())
            {
                return context.Questions.Any(x => x.Id == id);
            }

        }

        public bool Delete(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                Question currentUser = ctx.Questions.AsNoTracking().SingleOrDefault(x => x.Id == id);

                if (currentUser == null) return false;

                currentUser.IsActive = false;
                currentUser.ModifiedDate = DateTime.Now;

                ctx.SaveChanges();

                return true;
            }
        }


        //public int Add(User user)
        //{
        //    using (var ctx = new UserManagementContext())
        //    {
        //        user.CreatedDate = DateTime.Now;
        //        user.ModifiedDate = null;
        //        user.IsActive = true;

        //        ctx.Users.Add(user);

        //        ctx.SaveChanges();

        //        return user.Id;
        //    }
        //}

        //public bool Delete(int id)
        //{
        //    using (var ctx = new UserManagementContext())
        //    {
        //        User currentUser = ctx.Users.AsNoTracking().SingleOrDefault(x => x.Id == id);

        //        if (currentUser == null) return false;

        //        currentUser.IsActive = false;

        //        ctx.Entry(currentUser).State = EntityState.Modified;

        //        ctx.SaveChanges();

        //        return true;
        //    }
        //}

        //public bool Exist(string @is)
        //{
        //    using (var ctx = new UserManagementContext())
        //    {
        //        return ctx.Users.AsNoTracking().Any(x => x.IS.ToLower() == @is.ToLower());
        //    }
        //}

        //public bool Exist(int userId, bool includeInactive = false)
        //{
        //    using (var context = new UserManagementContext())
        //    {
        //        return context.Users.Any(x => x.Id == userId && (includeInactive || (!includeInactive && x.IsActive)));
        //    }
        //}

        //public User Get(int id)
        //{
        //    using (var ctx = new UserManagementContext())
        //    {
        //        return ctx.Users.AsNoTracking().SingleOrDefault(x => x.Id == id);
        //    }
        //}

        //public string GetIS(int id)
        //{
        //    using (var ctx = new UserManagementContext())
        //    {
        //        return ctx.Users.SingleOrDefault(x => x.Id == id).IS ?? null;
        //    }
        //}

        //public bool Update(User user)
        //{
        //    using (var ctx = new UserManagementContext())
        //    {
        //        User currentUser = ctx.Users.SingleOrDefault(x => x.Id == user.Id);

        //        if (currentUser == null) return false;

        //        currentUser.IS = user.IS;
        //        currentUser.FirstName = user.FirstName;
        //        currentUser.LastName = user.LastName;
        //        currentUser.DateOfBirth = user.DateOfBirth;
        //        currentUser.Salary = user.Salary;
        //        currentUser.ModifiedDate = DateTime.Now;

        //        ctx.Entry(currentUser).State = EntityState.Modified;

        //        ctx.SaveChanges();

        //        return true;
        //    }
        //}
    }
}
